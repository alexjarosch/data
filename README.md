# data

Data files used by the scikit-image project

`Normal_Epidermis_and_Dermis_with_Intradermal_Nevus_10x.JPG`

- origin: https://en.wikipedia.org/wiki/File:Normal_Epidermis_and_Dermis_with_Intradermal_Nevus_10x.JPG
- license: public domain
- description: hematoxylin and eosin stained slide at 10x of normal epidermis and dermis with a benign intradermal nevus

`AS_09125_050116030001_D03f00d0.tif`

- origin: https://github.com/CellProfiler/examples/blob/master/ExampleHuman/images/AS_09125_050116030001_D03f00d0.tif
- license: CC0
- description: microscopy image of human cells provided by Jason Moffat
through [CellProfiler](https://cellprofiler.org/examples/#human-cells):
Moffat J, Grueneberg DA, Yang X, Kim SY, Kloepfer AM, Hinkle G, Piqani
B, Eisenhaure TM, Luo B, Grenier JK, Carpenter AE, Foo SY, Stewart SA,
Stockwell BR, Hacohen N, Hahn WC, Lander ES, Sabatini DM, Root DE
(2006) "A lentiviral RNAi library for human and mouse genes applied to
an arrayed viral high-content screen" Cell, 124(6):1283-98.
PMID: 16564017
:DOI:`10.1016/j.cell.2006.01.040`

`kidney-tissue-fluorescence.tif`

- origin: Image acquired by Genevieve Buckley at Monasoh Micro Imaging in 2018.
- license: CC0
- description: Mouse kidney tissue on a pre-prepared slide imaged with confocal fluorescence microscopy (Nikon C1 inverted microscope). Image shape is (16, 512, 512, 3). That is 512x512 pixels in X-Y, 16 image slices in Z, and 3 color channels (emission wavelengths 450nm, 515nm, and 605nm, respectively). Real space voxel size is 1.24 microns in X-Y, and 1.25 microns in Z. Data type is unsigned 16-bit integers.

`lily-of-the-valley-fluorescence.tif`

- origin: Image acquired by Genevieve Buckley at Monasoh Micro Imaging in 2018.
- license: CC0
- description: Lily of the valley plant stem on a pre-prepared slide imaged with confocal fluorescence microscopy (Nikon C1 inverted microscope). Image shape is (922, 922, 4). That is 922x922 pixels in X-Y, with 4 color channels. Real space voxel size is 1.24 microns in X-Y. Data type is unsigned 16-bit integers.

`astronaut_rl.npy`

- description: testdata for [skimage/restoration/tests/test_restoration](https://github.com/scikit-image/scikit-image/blob/master/skimage/restoration/tests/test_restoration.py)

`brain.tiff`

- description: Image shape is (10, 256, 256). That is 256x256 pixels in X-Y,
and 10 slices in Z.

`eagle.png`

- origin: Image acquired by Dayane Machado at the Prague Castle in 2019.
- license: CC0
- description: Golden eagle. Image shape is (2019, 1826).
